# Kominal Core UserService Angular Client

Please note that using this components requires running a [Kominal Core UserService](https://gitlab.com/kominal/core/user-service).

# Setup

1. Add routing configuration for `/authentication`

In your `app-routing.module.ts` add the following route:

```
{
	path: 'authentication',
	component: AuthenticationComponent,
	loadChildren: () => import('@kominal/core-user-service-angular-client').then((m) => m.AuthenticationModule),
},
```

2. Add authentication interceptor

In your `app.module.ts` add the following to the `providers` array:

```
{
	provide: HTTP_INTERCEPTORS,
	useClass: AuthenticationInterceptor,
	multi: true,
},
```

3. Add configuration for CoreUserServiceModule

In your `app.module.ts` add the following to the `imports` array:

```
CoreUserServiceModule.forRoot({
	baseUrl: 'https://<projectName>.kominal.app',
	privateEndpoints: ['/controller', '/user-service/user'],
	publicEndpoints: ['/user-service/user/register'],
	loginRedirectUrl: '/dashboard',
	logoutRedirectUrl: '/',
}),
```

# Futher information

- When using multiple modules you can add `CoreUserServiceModule` to the corresponding `imports` array to use services and components from the library.

# Usage

1. Route protection

Routes can be protected using the build-in mutli-tenant permission system and guards:

UserGuard

The UserGuard will extract the `tenantId` from the URL for tenant identification.
It ensures that the user is logged in and at least one of the listed permissions is granted.

```
{
	path: 'tenant/:tenantId/settings',
	component: SettingsComponent,
	loadChildren: () => import('./modules/settings/settings.module').then((m) => m.SettingsModule),
	canActivate: [UserGuard],
	data: {
	permissions: ['<custom>.<permission>'],
	},
},
```

GuestGuard

The GuestGuard ensures only visiters that are not logged in can load a route.

```
	{
		path: 'authentication',
		component: AuthenticationComponent,
		loadChildren: () => import('./modules/authentication/authentication.module').then((m) => m.AuthenticationModule),
		canActivate: [GuestGuard],
	},
```
