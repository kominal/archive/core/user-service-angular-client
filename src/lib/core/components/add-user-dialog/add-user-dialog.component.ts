import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
	selector: 'lib-add-user-dialog',
	templateUrl: './add-user-dialog.component.html',
	styleUrls: ['./add-user-dialog.component.scss'],
})
export class AddUserDialogComponent {
	form: FormGroup;

	constructor(public dialogRef: MatDialogRef<AddUserDialogComponent>) {
		this.form = new FormGroup({
			email: new FormControl(undefined, Validators.required),
		});
	}

	onSubmit() {
		this.dialogRef.close(this.form.value.email);
	}
}
