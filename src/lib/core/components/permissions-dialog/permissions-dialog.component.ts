import { Component, Inject, InjectionToken } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject } from 'rxjs';

export const PERMISSIONS = new InjectionToken<BehaviorSubject<string[]> | undefined>('PERMISSIONS');

@UntilDestroy()
@Component({
	selector: 'lib-permissions-dialog',
	templateUrl: './permissions-dialog.component.html',
	styleUrls: ['./permissions-dialog.component.scss'],
})
export class PermissionsDialogComponent {
	form: FormGroup;

	availablePermissions = new BehaviorSubject<string[]>(['core.user-service.write', 'core.user-service.read']);

	constructor(
		public dialogRef: MatDialogRef<PermissionsDialogComponent>,
		@Inject(PERMISSIONS) customPermissions: BehaviorSubject<string[]> | undefined,
		@Inject(MAT_DIALOG_DATA) permissions: string[]
	) {
		customPermissions?.pipe(untilDestroyed(this)).subscribe((customPermissions) => {
			this.availablePermissions.next(['core.user-service.write', 'core.user-service.read', ...customPermissions]);
		});
		this.form = new FormGroup({
			permissions: new FormControl(permissions),
		});
	}

	onSubmit() {
		this.dialogRef.close(this.form.value.permissions);
	}
}
