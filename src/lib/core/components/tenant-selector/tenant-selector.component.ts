import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { filter, first, switchMap } from 'rxjs/operators';
import { BaseTenant } from '../../models/tenant';
import { TenantService } from '../../services/tenant.service';
import { UserService } from '../../services/user.service';
@Component({
	selector: 'lib-tenant-selector',
	templateUrl: './tenant-selector.component.html',
	styleUrls: ['./tenant-selector.component.scss'],
})
export class TenantSelectorComponent {
	@Input() tenantCreationRouterLink = '/authentication/tenants/create';

	constructor(public tenantService: TenantService<BaseTenant>, public userService: UserService, private router: Router) {}

	async delete() {
		this.userService.currentTenant$
			.pipe(
				first((tenant) => !!tenant),
				switchMap((tenant) => this.tenantService.delete(tenant!, 'name')),
				filter((r) => !!r)
			)
			.subscribe(() => {
				this.router.navigate(['/']);
			});
	}
}
