import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { GUEST_HOME_URL, UserService } from '../../services/user.service';

@Injectable({
	providedIn: 'root',
})
export class UserGuard implements CanActivate {
	constructor(private userService: UserService, private router: Router, @Inject(GUEST_HOME_URL) private guestHomeUrl: string | undefined) {}

	canActivate(route: ActivatedRouteSnapshot): boolean | Observable<boolean> {
		if (!this.userService.isLoggedIn()) {
			this.router.navigate([this.guestHomeUrl || '/']);
			return false;
		}

		if (!route.data?.permissions) {
			return true;
		}

		let permissions = route.data?.permissions as string[];

		return this.userService.currentPermissions$.pipe(
			filter((userPermissions) => !!userPermissions),
			map((currentPermissions) => {
				for (const permission of permissions) {
					if (currentPermissions?.includes(permission)) {
						return true;
					}
				}
				return false;
			})
		);
	}
}
