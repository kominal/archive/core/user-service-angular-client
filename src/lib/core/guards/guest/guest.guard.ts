import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { UserService, USER_HOME_URL } from '../../services/user.service';

@Injectable({
	providedIn: 'root',
})
export class GuestGuard implements CanActivate {
	constructor(private userService: UserService, private router: Router, @Inject(USER_HOME_URL) private userHomeUrl: string | undefined) {}

	async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
		if (this.userService.isLoggedIn()) {
			this.router.navigate([this.userHomeUrl || '/']);
			return false;
		}
		return true;
	}
}
