import { ChangeDetectorRef, Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

@Directive({
	selector: '[permissions]',
})
export class PermissionsDirective implements OnInit, OnDestroy {
	@Input() permissions!: string[];

	private subscription: Subscription | undefined;

	constructor(
		private userService: UserService,
		private viewContainer: ViewContainerRef,
		private changeDetector: ChangeDetectorRef,
		private templateRef: TemplateRef<any>
	) {}

	ngOnInit(): void {
		this.viewContainer.clear();
		this.subscription = this.userService.currentPermissions$
			.pipe(filter((userPermissions) => !!userPermissions))
			.subscribe((userPermissions) => {
				let allowed = false;
				for (let permission of this.permissions) {
					if (userPermissions?.includes(permission)) {
						allowed = true;
						break;
					}
				}

				if (allowed && this.viewContainer.length === 0) {
					this.viewContainer.createEmbeddedView(this.templateRef);
					this.changeDetector.markForCheck();
				} else if (!allowed && this.viewContainer.length > 0) {
					this.viewContainer.clear();
					this.changeDetector.markForCheck();
				}
			});
	}

	ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}
}
