export interface Payload {
	permissions: string[];
	tenants: {
		[key: string]: {
			permissions: string[];
		};
	};
}
