export interface BaseTenant {
	_id: string;
	name: string;
	[key: string]: any;
}
