import { Inject, Injectable, InjectionToken } from '@angular/core';
import { ActivatedRouteSnapshot, ChildActivationStart, Params, Router } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { hash } from 'bcryptjs';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { concatMap, distinctUntilChanged, filter, map, shareReplay, switchAll, switchMap } from 'rxjs/operators';
import { generateToken } from '../classes/helper';
import { UserHttpService } from '../http/user-http.service';
import { JWT } from '../models/jwt';
import { BaseTenant } from '../models/tenant';
import { User } from '../models/user';
import { TenantService, TENANT_IDENTIFIER_NAME } from './tenant.service';

export const GUEST_HOME_URL = new InjectionToken<string | undefined>('GUEST_HOME_URL');
export const USER_HOME_URL = new InjectionToken<string | undefined>('USER_HOME_URL');
export const CHANGE_PASSWORD_REDIRECT_URL = new InjectionToken<string | undefined>('CHANGE_PASSWORD_REDIRECT_URL');
export const SINGLE_TENANT_MODE = new InjectionToken<boolean | undefined>('CORE_SINGLE_TENANT_MODE');

@Injectable({
	providedIn: 'root',
})
export class UserService<T extends BaseTenant = BaseTenant> {
	private SALT = '$2a$10$BGS4LQdHK7nFhRvW5YM7b.';

	public loginSubject = new Subject();
	public tokenSubject = new BehaviorSubject<(JWT & { jwtReceived: number }) | undefined>(undefined);
	public logoutSubject = new Subject();

	public tenants$ = new Observable<T[]>();
	public reloadTenants$ = new BehaviorSubject<void>(undefined);
	public currentTenantIdentifier$ = new Observable<string | undefined>();
	public currentTenant$ = new Observable<T | undefined>();
	public reloadCurrentTenant$ = new BehaviorSubject<void>(undefined);

	public currentPermissions$ = new Observable<string[] | undefined>();

	private refreshing = new BehaviorSubject<boolean>(false);

	constructor(
		private userHttpService: UserHttpService,
		@Inject(GUEST_HOME_URL) private guestHomeUrl: string | undefined,
		@Inject(USER_HOME_URL) private userHomeUrl: string | undefined,
		@Inject(CHANGE_PASSWORD_REDIRECT_URL) private changePasswordRedirectUrl: string | undefined,
		@Inject(TENANT_IDENTIFIER_NAME) tenantIdentifierName: string | undefined,
		@Inject(SINGLE_TENANT_MODE) singleTenantMode: boolean | undefined,
		private tenantService: TenantService<T>,
		private logService: LogService,
		private router: Router
	) {
		this.tenants$ = combineLatest([this.tokenSubject, this.reloadTenants$]).pipe(
			distinctUntilChanged(([p], [n]) => {
				if ((!!p && !n) || (!p && !!n)) {
					return false;
				}
				return Object.keys(p?.payload.tenants || {}).join(',') === Object.keys(n?.payload.tenants || {}).join(',');
			}),
			switchMap(([token]) => (token ? this.tenantService.list().pipe(map((r) => r.items)) : of([]))),
			shareReplay(1)
		);

		this.currentTenantIdentifier$ = router.events.pipe(
			filter((e) => e instanceof ChildActivationStart),
			map((e) => {
				/*
				console.log(
					e instanceof NavigationStart,
					e instanceof RouteConfigLoadStart,
					e instanceof RouteConfigLoadEnd,
					e instanceof RoutesRecognized,
					e instanceof GuardsCheckStart,
					e instanceof ChildActivationStart, // tenantIdentifier
					e instanceof ActivationStart, // tenantIdentifier
					e instanceof GuardsCheckEnd,
					e instanceof ResolveStart,
					e instanceof ResolveEnd,
					e instanceof ChildActivationEnd,
					e instanceof ActivationEnd, // snapshot
					e instanceof NavigationEnd, // snapshot
					e instanceof NavigationCancel,
					e instanceof NavigationError,
					e instanceof Scroll
				);
				*/

				if (e instanceof ChildActivationStart) {
					const params: Params = {};
					const collectParams = (snapshot: ActivatedRouteSnapshot): any => {
						Object.assign(params, snapshot.params);
						snapshot.children.forEach(collectParams);
					};
					collectParams(e.snapshot);
					return params;
				}
				return {};
			}),
			map(({ tenantIdentifier }) => tenantIdentifier),
			distinctUntilChanged(),
			shareReplay(1)
		);

		this.currentTenant$ = combineLatest([this.currentTenantIdentifier$, this.reloadCurrentTenant$]).pipe(
			concatMap(([tenantIdentifier]) =>
				tenantIdentifier ? [of(undefined), this.tenantService.getByIdentifier(tenantIdentifier!)] : [of(undefined)]
			),
			switchAll(),
			shareReplay(1)
		);
		this.currentTenant$.subscribe();

		this.currentPermissions$ = combineLatest([this.currentTenant$, this.tokenSubject]).pipe(
			map(([tenant, token]) => {
				if (token) {
					if (singleTenantMode) {
						return token.payload.permissions;
					} else if (tenant && token.payload.tenants[tenant._id]) {
						return token.payload.tenants[tenant._id].permissions;
					}
				}
				return undefined;
			})
		);
		if (this.isLoggedIn()) {
			this.refresh();
		}
	}

	async register(email: string, password: string) {
		await this.userHttpService.register(location.origin, email, await hash(password, this.SALT));
		this.logService.info('Registration completed', 'transl.simple.verifyEmail');
		this.router.navigate(['/authentication/login']);
	}

	async verifyEmail(userId: string, token: string, options?: { redirect?: boolean }): Promise<void> {
		const key = generateToken(32);

		await this.userHttpService.verifyEmail(userId, token, `${navigator.platform}, ${navigator.appCodeName}`, await hash(key, this.SALT));

		localStorage.setItem('sessionKey', key);

		await this.refresh();

		if (options?.redirect === true) {
			this.router.navigate([this.userHomeUrl || '/']);
		}
	}

	async login(email: string, password: string, options?: { redirect?: boolean }) {
		const key = generateToken(32);

		await this.userHttpService.createSession(
			email,
			await hash(password, this.SALT),
			`${navigator.platform}, ${navigator.appCodeName}`,
			await hash(key, this.SALT)
		);

		localStorage.setItem('sessionKey', key);

		await this.refresh();

		if (options?.redirect === true) {
			this.router.navigate([this.userHomeUrl || '/']);
		}
	}

	async refresh(): Promise<void> {
		const sessionKey = localStorage.getItem('sessionKey');
		if (!sessionKey) {
			throw new Error(`Can't refresh when not logged in.`);
		}

		if (this.refreshing.value) {
			return new Promise((resolve) => this.refreshing.pipe(filter((v) => !v)).subscribe((v) => resolve()));
		}

		this.refreshing.next(true);
		try {
			this.logService.info('Refreshing session...');

			const jwt = await this.userHttpService.refreshSession(await hash(sessionKey, this.SALT));
			this.tokenSubject.next({ ...jwt, jwtReceived: Date.now() });
		} catch (error) {
			this.logService.handleError(error);
			await this.logout();
		}

		this.refreshing.next(false);
	}

	async getJWT(): Promise<string> {
		let token = this.tokenSubject.value;
		if (!token || Date.now() > token.jwtReceived + (token.jwtExpires - token.jwtCreated) - 15000) {
			await this.refresh();

			token = this.tokenSubject.value;
			if (!token || Date.now() > token.jwtReceived + (token.jwtExpires - token.jwtCreated) - 15000) {
				throw new Error('JWT is undefined');
			}
		}

		return token.jwt;
	}

	isLoggedIn(): boolean {
		return !!localStorage.getItem('sessionKey');
	}

	async changePassword(currentPassword: string, newPassword: string): Promise<void> {
		await this.userHttpService.changePassword(await hash(currentPassword, this.SALT), await hash(newPassword, this.SALT));

		this.logService.info('Password changed', 'transl.simple.passwordChanged');
		this.router.navigate([this.changePasswordRedirectUrl || '/']);
	}

	async requestPasswordReset(email: string): Promise<void> {
		await this.userHttpService.requestPasswordReset(location.origin, email);

		this.logService.info('Password changed', 'transl.simple.passwordResetRequested');
		this.router.navigate(['/authentication/login']);
	}

	async requestEmailVerify(email: string): Promise<void> {
		await this.userHttpService.requestEmailVerify(location.origin, email);

		this.logService.info('Verification email requested', 'transl.simple.verificationEmailRequested');
		this.router.navigate(['/authentication/login']);
	}

	async resetPassword(userId: string, token: string, password: string): Promise<void> {
		return this.userHttpService.resetPassword(userId, token, await hash(password, this.SALT));
	}

	async logout(): Promise<void> {
		this.logService.info(`Logging you out...`, 'transl.simple.logout');

		try {
			const sessionKey = localStorage.getItem('sessionKey');
			localStorage.removeItem('sessionKey');

			this.router.navigate([this.guestHomeUrl || '/']);
			this.tokenSubject.next(undefined);
			this.logoutSubject.next();
			if (sessionKey) {
				await this.userHttpService.removeSession(await hash(sessionKey, this.SALT));
			}
		} catch (error) {
			this.logService.handleError(error);
		}
	}

	getUsers(pagination?: PaginationRequest, tenantId?: string): Observable<PaginationResponse<User>> {
		return this.userHttpService.getUsers(pagination, tenantId);
	}

	listPermissions(userId: string, tenantId?: string): Promise<string[]> {
		return this.userHttpService.listPermissions(userId, tenantId);
	}

	setPermissions(userId: string, permissions: string[], tenantId?: string): Promise<string[]> {
		return this.userHttpService.setPermissions(userId, permissions, tenantId);
	}

	getUser(userId: string): Promise<{ _id: string; email: string }> {
		return this.userHttpService.getUser(userId);
	}

	findUserByEmail(email: string): Promise<{ _id: string; email: string }> {
		return this.userHttpService.findUserByEmail(email);
	}

	deleteUser(userId: string, tenantId?: string): Promise<void> {
		return this.userHttpService.deleteUser(userId, tenantId);
	}
}
