import { Injectable, InjectionToken } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { ObserverClientService } from '@kominal/observer-angular-client';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { TenantHttpService } from '../http/tenant-http.service';
import { BaseTenant } from '../models/tenant';

export const TENANT_IDENTIFIER_NAME = new InjectionToken<string | undefined>('TENANT_IDENTIFIER_NAME');

@Injectable({
	providedIn: 'root',
})
export class TenantService<T extends BaseTenant = BaseTenant> {
	constructor(
		private httpService: TenantHttpService<T>,
		private dialogService: DialogService,
		protected observerClientService: ObserverClientService
	) {}

	list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<T>> {
		return this.httpService.list(paginationRequest).pipe(
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	get(tenantId: string): Observable<T | undefined> {
		return this.httpService.get(tenantId).pipe(
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of(undefined);
			})
		);
	}

	getByIdentifier(tenantIdentifier: string): Observable<T | undefined> {
		return this.httpService.getByIdentifier(tenantIdentifier).pipe(
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of(undefined);
			})
		);
	}

	create(tenant: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return this.httpService.create(tenant).pipe(
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of<false>(false);
			})
		);
	}

	update(tenant: Partial<T>): Observable<boolean> {
		return this.httpService.update(tenant).pipe(
			map(() => true),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of(false);
			})
		);
	}

	createOrUpdate(tenant: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	delete(document: Partial<T> & { _id: T['_id'] }, identifier: string, options?: { confirm?: boolean }): Observable<boolean> {
		const deleteObservable = this.httpService.delete(document._id).pipe(
			map(() => true),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of<false>(false);
			})
		);
		if (options?.confirm) {
			return this.dialogService.openConfirmDeleteDialog((document as any)[identifier || '_id'] || '').pipe(
				filter((v) => v),
				switchMap(() => deleteObservable)
			);
		} else {
			return deleteObservable;
		}
	}
}
