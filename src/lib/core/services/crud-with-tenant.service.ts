import { DialogService } from '@kominal/lib-angular-dialog';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { ObserverClientService } from '@kominal/observer-angular-client';
import { Observable, of } from 'rxjs';
import { catchError, filter, first, map, switchMap } from 'rxjs/operators';
import { CrudWithTenantHttpService } from '../http/crud-with-tenant-http.service';
import { UserService } from './user.service';

export class CrudWithTenantService<T extends { _id: string }, H extends CrudWithTenantHttpService<T> = CrudWithTenantHttpService<T>> {
	constructor(
		protected httpService: H,
		protected userService: UserService,
		protected dialogService: DialogService,
		protected observerClientService: ObserverClientService
	) {}

	list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<T>> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.list(tenant!._id, paginationRequest)),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	get(documentId: string): Observable<T | undefined> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.get(tenant!._id, documentId)),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of(undefined);
			})
		);
	}

	create(document: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.create(tenant!._id, document)),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of<false>(false);
			})
		);
	}

	update(document: Partial<T>): Observable<boolean> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.update(tenant!._id, document)),
			map(() => true),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of<false>(false);
			})
		);
	}

	createOrUpdate(tenant: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	delete(document: Partial<T> & { _id: T['_id'] }, identifier: string, options?: { confirm?: boolean }): Observable<boolean> {
		const deleteObservable = this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.delete(tenant!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.observerClientService.handleError(e);
				return of<false>(false);
			})
		);
		if (options?.confirm) {
			return this.dialogService.openConfirmDeleteDialog((document as any)[identifier || '_id'] || '').pipe(
				filter((v) => v),
				switchMap(() => deleteObservable)
			);
		} else {
			return deleteObservable;
		}
	}
}
