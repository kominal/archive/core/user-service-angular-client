import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED } from '../classes/helper';
import { JWT } from '../models/jwt';
import { User } from '../models/user';

export const BASE_URL = new InjectionToken<string | undefined>('BASE_URL');

@Injectable({
	providedIn: 'root',
})
export class UserHttpService {
	constructor(@Inject(BASE_URL) private baseUrl: string | undefined, private httpClient: HttpClient) {}

	private getServiceBaseUrl() {
		return `${this.baseUrl || 'https://core.kominal.app'}/user-service`;
	}

	register(baseUrl: string, email: string, password: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceBaseUrl()}/user/register`, { baseUrl, email, password })
			.toPromise();
	}

	createSession(email: string, password: string, device: string, key: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceBaseUrl()}/sessions`, { email, password, device, key })
			.toPromise();
	}

	refreshSession(key: string): Promise<JWT> {
		return this.httpClient
			.get<any>(`${this.getServiceBaseUrl()}/sessions`, {
				headers: {
					Authorization: `Bearer ${key}`,
				},
			})
			.toPromise();
	}

	removeSession(key: string): Promise<void> {
		return this.httpClient
			.delete<void>(`${this.getServiceBaseUrl()}/sessions`, {
				headers: {
					Authorization: `Bearer ${key}`,
				},
			})
			.toPromise();
	}

	requestPasswordReset(baseUrl: string, email: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceBaseUrl()}/password/requestReset`, { baseUrl, email })
			.toPromise();
	}

	resetPassword(userId: string, token: string, password: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceBaseUrl()}/password/reset`, { userId, token, password })
			.toPromise();
	}

	requestEmailVerify(baseUrl: string, email: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceBaseUrl()}/email/requestVerify`, { baseUrl, email })
			.toPromise();
	}

	verifyEmail(userId: string, token: string, device: string, key: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceBaseUrl()}/email/verify`, { userId, token, device, key })
			.toPromise();
	}

	changePassword(currentPassword: string, newPassword: string): Promise<void> {
		return this.httpClient
			.put<void>(
				`${this.getServiceBaseUrl()}/password/change`,
				{
					currentPassword,
					newPassword,
				},
				{ headers: AUTHENTICATION_REQUIRED }
			)
			.toPromise();
	}

	getUsers(paginationRequest?: PaginationRequest, tenantId?: string): Observable<PaginationResponse<User>> {
		return this.httpClient.get<PaginationResponse<User>>(`${this.getServiceBaseUrl()}/users`, {
			params: this.toParams({ tenantId, ...toPaginationParams(paginationRequest) }),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	listPermissions(userId: string, tenantId?: string): Promise<string[]> {
		return this.httpClient
			.get<string[]>(`${this.getServiceBaseUrl()}/user/${userId}/permissions`, {
				params: this.toParams({ tenantId }),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	setPermissions(userId: string, permissions: string[], tenantId?: string): Promise<string[]> {
		return this.httpClient
			.put<string[]>(
				`${this.getServiceBaseUrl()}/user/${userId}/permissions`,
				{ permissions },
				{
					params: this.toParams({ tenantId }),
					headers: AUTHENTICATION_REQUIRED,
				}
			)
			.toPromise();
	}

	getUser(userId: string): Promise<{ _id: string; email: string }> {
		return this.httpClient
			.get<{ _id: string; email: string }>(`${this.getServiceBaseUrl()}/users/${userId}`, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	findUserByEmail(email: string): Promise<{ _id: string; email: string }> {
		return this.httpClient
			.get<{ _id: string; email: string }>(`${this.getServiceBaseUrl()}/users/findByEmail/${encodeURIComponent(email)}`, {
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	deleteUser(userId: string, tenantId?: string): Promise<void> {
		return this.httpClient
			.delete<void>(`${this.getServiceBaseUrl()}/users/${userId}`, {
				params: this.toParams({ tenantId }),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	toParams(params: { [param: string]: any }): { [param: string]: string } | undefined {
		const cleanedParams: { [param: string]: string } = {};
		for (const key of Object.keys(params)) {
			if (params[key]) {
				cleanedParams[key] = params[key];
			}
		}
		return Object.keys(cleanedParams).length > 0 ? cleanedParams : undefined;
	}
}
