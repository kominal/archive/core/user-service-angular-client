import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { CrudHttpService } from './crud-http.service';

@Injectable({
	providedIn: 'root',
})
export class TenantHttpService<T extends { _id: string }> extends CrudHttpService<T> {
	constructor(httpClient: HttpClient) {
		super(httpClient, '/controller', 'tenants');
	}

	getByIdentifier(documentIdentifier: string): Observable<T> {
		return this.httpClient.get<T>(
			`${this.baseUrl}/${this.collection}/by-identifier/${documentIdentifier}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}
