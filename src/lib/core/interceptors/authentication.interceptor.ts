import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { from, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { AUTHENTICATION_REQUIRED_HEADER } from '../classes/helper';
import { UserService } from '../services/user.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
	constructor(private injector: Injector) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (!req.headers.has(AUTHENTICATION_REQUIRED_HEADER)) {
			return next.handle(req);
		}

		const headers = req.headers.delete(AUTHENTICATION_REQUIRED_HEADER);
		return from(this.injector.get(UserService).getJWT()).pipe(
			mergeMap((jwt) => {
				return next.handle(
					req.clone({
						headers,
						setHeaders: {
							Authorization: `Bearer ${jwt}`,
						},
					})
				);
			})
		);
	}
}
