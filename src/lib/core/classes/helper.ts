export function generateToken(length: number) {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

export const AUTHENTICATION_REQUIRED_HEADER = 'X-AUTHENTICATION-REQUIRED';

export const AUTHENTICATION_REQUIRED = {
	[AUTHENTICATION_REQUIRED_HEADER]: 'true',
};

export const AUTHENTICATION_REQUIRED_OPTIONS = {
	headers: AUTHENTICATION_REQUIRED,
};
