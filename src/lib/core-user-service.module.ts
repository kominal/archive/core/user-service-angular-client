import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TableModule } from '@kominal/lib-angular-table';
import { TranslateModule } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { AddUserDialogComponent } from './core/components/add-user-dialog/add-user-dialog.component';
import { PERMISSIONS, PermissionsDialogComponent } from './core/components/permissions-dialog/permissions-dialog.component';
import { RolesDialogComponent } from './core/components/roles-dialog/roles-dialog.component';
import { TenantSelectorComponent } from './core/components/tenant-selector/tenant-selector.component';
import { PermissionsDirective } from './core/directives/permissions/permissions.directive';
import { GuestGuard } from './core/guards/guest/guest.guard';
import { UserGuard } from './core/guards/user/user.guard';
import { BASE_URL, UserHttpService } from './core/http/user-http.service';
import { AuthenticationInterceptor } from './core/interceptors/authentication.interceptor';
import { PermissionsPipe } from './core/pipes/permissions/permissions.pipe';
import { TENANT_IDENTIFIER_NAME } from './core/services/tenant.service';
import { CHANGE_PASSWORD_REDIRECT_URL, GUEST_HOME_URL, SINGLE_TENANT_MODE, UserService, USER_HOME_URL } from './core/services/user.service';
import { MaterialModule } from './shared/material.module';

export function initFactory() {
	const x = 0;
	return () => {};
}

@NgModule({
	declarations: [
		PermissionsDirective,
		PermissionsDialogComponent,
		AddUserDialogComponent,
		RolesDialogComponent,
		TenantSelectorComponent,
		PermissionsPipe,
	],
	imports: [CommonModule, ReactiveFormsModule, HttpClientModule, FormsModule, MaterialModule, TranslateModule, TableModule, RouterModule],
	exports: [PermissionsDirective, PermissionsDialogComponent, AddUserDialogComponent, TenantSelectorComponent, PermissionsPipe],
})
export class CoreUserServiceModule {
	static forRoot(config?: {
		baseUrl?: string;
		guestHomeUrl?: string;
		userHomeUrl?: string;
		changePasswordRedirectUrl?: string;
		permissions?: BehaviorSubject<string[]>;
		rolesEnabled?: boolean;
		tenantIdentifierName?: string;
		singleTenantMode?: boolean;
	}): ModuleWithProviders<CoreUserServiceModule> {
		return {
			ngModule: CoreUserServiceModule,
			providers: [
				{ provide: BASE_URL, useValue: config?.baseUrl },
				{ provide: GUEST_HOME_URL, useValue: config?.guestHomeUrl },
				{ provide: USER_HOME_URL, useValue: config?.userHomeUrl },
				{ provide: CHANGE_PASSWORD_REDIRECT_URL, useValue: config?.changePasswordRedirectUrl },
				{ provide: PERMISSIONS, useValue: config?.permissions },
				{ provide: TENANT_IDENTIFIER_NAME, useValue: config?.tenantIdentifierName },
				{ provide: SINGLE_TENANT_MODE, useValue: config?.singleTenantMode },
				UserService,
				UserGuard,
				GuestGuard,
				UserHttpService,
				PermissionsPipe,
				AuthenticationInterceptor,
				{
					provide: HTTP_INTERCEPTORS,
					useClass: AuthenticationInterceptor,
					multi: true,
				},
				{
					provide: APP_INITIALIZER,
					useFactory: initFactory,
					deps: [UserService],
					multi: true,
				},
			],
		};
	}
}
