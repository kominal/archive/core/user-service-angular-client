import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormModule } from '@kominal/lib-angular-form';
import { TranslateModule } from '@ngx-translate/core';
import { CoreUserServiceModule } from '../../core-user-service.module';
import { MaterialModule } from '../../shared/material.module';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RequestPasswordResetComponent } from './pages/request-password-reset/request-password-reset.component';
import { RequestVerificationEmailComponent } from './pages/request-verification-email/request-verification-email.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { RolesComponent } from './pages/roles/roles.component';
import { TenantComponent } from './pages/tenant/tenant.component';
import { UsersComponent } from './pages/users/users.component';

@NgModule({
	declarations: [
		AuthenticationComponent,
		ChangePasswordComponent,
		LoginComponent,
		RegisterComponent,
		ResetPasswordComponent,
		RolesComponent,
		UsersComponent,
		RequestPasswordResetComponent,
		RequestVerificationEmailComponent,
		TenantComponent,
	],
	imports: [
		CommonModule,
		AuthenticationRoutingModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		FormModule,
		TranslateModule,
		CoreUserServiceModule,
	],
})
export class AuthenticationModule {}
