import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { UserService } from '../../../../core/services/user.service';

@Component({
	selector: 'lib-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent {
	loading = false;

	form = new FormGroup({
		password: new FormControl(undefined, [Validators.required, Validators.maxLength(256)]),
	});

	constructor(public userService: UserService, private activatedRoute: ActivatedRoute, private logService: LogService) {}

	async onSubmit(): Promise<void> {
		this.loading = true;

		const { password } = this.form.value;
		const { userId, token } = this.activatedRoute.snapshot.queryParams;

		try {
			if (userId && token) {
				await this.userService.resetPassword(userId, token, password);
			} else {
				this.logService.info('Missing required parameters.', 'transl.errer.missingParameter');
			}
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}
}
