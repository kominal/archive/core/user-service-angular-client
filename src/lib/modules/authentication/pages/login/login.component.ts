import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { UserService } from '../../../../core/services/user.service';

@Component({
	selector: 'lib-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
	loading = false;

	form = new FormGroup({
		email: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(256)]),
		password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(256)]),
	});

	constructor(public userService: UserService, private activatedRoute: ActivatedRoute, private logService: LogService) {}

	ngOnInit(): void {
		this.activatedRoute.queryParams.subscribe(async (params) => {
			if (params.userId && params.token) {
				this.userService.verifyEmail(params.userId, params.token);
			}
		});
	}

	async onSubmit(): Promise<void> {
		this.loading = true;

		const { email, password } = this.form.value;
		try {
			await this.userService.login(email, password);
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}
}
