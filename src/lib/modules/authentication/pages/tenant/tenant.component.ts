import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseTenant } from '../../../../core/models/tenant';
import { TenantService } from '../../../../core/services/tenant.service';
import { UserService } from '../../../../core/services/user.service';

@Component({
	selector: 'lib-tenant',
	templateUrl: './tenant.component.html',
	styleUrls: ['./tenant.component.scss'],
})
export class TenantComponent {
	public Object = Object;

	public loading = false;
	public form = new FormGroup({
		_id: new FormControl(undefined),
		name: new FormControl(undefined, Validators.required),
	});

	constructor(public userService: UserService, public tenantService: TenantService<BaseTenant>, private router: Router) {
		this.userService.currentTenant$.subscribe((tenant) => {
			if (tenant) {
				this.form.patchValue(tenant);
			}
		});
	}

	async onSubmit() {
		this.loading = true;
		this.tenantService.createOrUpdate(this.form.value).subscribe((r) => {
			if (!r) {
				this.loading = false;
			} else {
				this.userService.tenants$.subscribe(() => {
					this.router.navigate(['/tenants/', r._id]);
				});
				this.userService.refresh();
			}
		});
	}
}
