import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LogService } from '@kominal/lib-angular-logging';
import { UserService } from '../../../../core/services/user.service';

@Component({
	selector: 'lib-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent {
	loading = false;

	form = new FormGroup({
		currentPassword: new FormControl(undefined, [Validators.required, Validators.maxLength(256)]),
		newPassword: new FormControl(undefined, [Validators.required, Validators.maxLength(256)]),
	});

	constructor(public userService: UserService, private logService: LogService) {}

	async onSubmit(): Promise<void> {
		this.loading = true;

		const { currentPassword, newPassword } = this.form.value;
		try {
			await this.userService.changePassword(currentPassword, newPassword);
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}
}
