import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LogService } from '@kominal/lib-angular-logging';
import { UserService } from '../../../../core/services/user.service';

@Component({
	selector: 'lib-request-password-reset',
	templateUrl: './request-password-reset.component.html',
	styleUrls: ['./request-password-reset.component.scss'],
})
export class RequestPasswordResetComponent {
	loading = false;

	form = new FormGroup({
		email: new FormControl(undefined, [Validators.required, Validators.email, Validators.maxLength(256)]),
	});

	constructor(public userService: UserService, private logService: LogService) {}

	async onSubmit(): Promise<void> {
		this.loading = true;

		const { email } = this.form.value;
		try {
			await this.userService.requestPasswordReset(email);
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}
}
