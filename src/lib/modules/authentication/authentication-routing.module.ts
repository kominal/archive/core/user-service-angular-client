import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuestGuard } from '../../core/guards/guest/guest.guard';
import { UserGuard } from '../../core/guards/user/user.guard';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RequestPasswordResetComponent } from './pages/request-password-reset/request-password-reset.component';
import { RequestVerificationEmailComponent } from './pages/request-verification-email/request-verification-email.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { RolesComponent } from './pages/roles/roles.component';
import { TenantComponent } from './pages/tenant/tenant.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent, canActivate: [GuestGuard] },
	{ path: 'register', component: RegisterComponent, canActivate: [GuestGuard] },
	{ path: 'request-password-reset', component: RequestPasswordResetComponent, canActivate: [GuestGuard] },
	{ path: 'request-verification-email', component: RequestVerificationEmailComponent, canActivate: [GuestGuard] },
	{ path: 'reset-password', component: ResetPasswordComponent, canActivate: [GuestGuard] },
	{ path: 'change-password', component: ChangePasswordComponent, canActivate: [UserGuard] },
	{ path: 'tenants/create', component: TenantComponent, canActivate: [UserGuard] },
	{ path: 'tenants/create', component: TenantComponent, canActivate: [UserGuard] },
	{ path: 'tenants/:tenantId', component: TenantComponent, canActivate: [UserGuard] },
	{
		path: 'users',
		component: UsersComponent,
		canActivate: [UserGuard],
		data: { permissions: ['core.user-service.write', 'core.user-service.read'] },
	},
	{
		path: 'tenants/:tenantId/users',
		component: UsersComponent,
		canActivate: [UserGuard],
		data: { permissions: ['core.user-service.write', 'core.user-service.read'] },
	},
	{
		path: 'roles',
		component: RolesComponent,
		canActivate: [UserGuard],
		data: { permissions: ['core.user-service.write', 'core.user-service.read'] },
	},
	{
		path: 'tenants/:tenantId/roles',
		component: RolesComponent,
		canActivate: [UserGuard],
		data: { permissions: ['core.user-service.write', 'core.user-service.read'] },
	},
	{ path: '**', redirectTo: 'login' },
];

export const router = RouterModule.forChild(routes);

@NgModule({
	imports: [router],
	exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
